{
    "cmsweb-dev": {
        "delegate-dn": [
            "/DC=ch/DC=cern/OU=computers/CN=vocms(3[136]|21|045|052|021|031|0118|0118a|0119).cern.ch"
        ],
        "backend-urls" : {
            "cacheSSL" : "https://cmsweb-testbed.cern.ch/crabcache",
            "baseURL" : "https://cmsweb-testbed.cern.ch/crabcache",
            "baseURLSSL" : "https://pandaserver.cern.ch:25443/server/panda",
            "htcondorSchedds" : ["crab3test-6@vocms20.cern.ch"],
            "htcondorPool" : "cmssrv221.fnal.gov",
            "ASOURL" : "https://cmsweb-testbed.cern.ch/couchdb"
        },
        "compatible-version" : ["3.3.9", "3.3.9.rc2"],
        "banned-out-destinations" : ["T1_CH_CERN_Disk", "T1_DE_KIT_Disk", "T1_ES_PIC_Disk", "T1_FR_CCIN2P3_Disk", "T1_IT_CNAF_Disk", "T1_RU_JINR_Disk", "T1_TW_ASGC_Disk", "T1_UK_RAL_Disk", "T1_US_FNAL_Disk"]
    },
    "cmsweb-preprod": {
        "delegate-dn": [
            "/DC=ch/DC=cern/OU=computers/CN=devaso2.cern.ch|/DC=ch/DC=cern/OU=computers/CN=vocms(045|052|021|031|030|0118|0118a|0119).cern.ch"
        ],
        "backend-urls" : {
            "cacheSSL" : "https://cmsweb-testbed.cern.ch/crabcache",
            "baseURL" : "https://cmsweb-testbed.cern.ch/crabcache",
            "htcondorSchedds" : {
                "crab3@vocms021.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/021"
                },
                "crab3-5@vocms059.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/059"
                },
                "crab3-4@vocms066.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/066"
                },
                "crab3@vocms0106.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0106"
                },
                "crab3@vocms0121.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0121"
                },
                "crab3@vocms0122.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0122"
                },
                "crab3@vocms0155.cern.ch" : {
                    "Proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0155"
                },
                "crab3@vocms0144.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0144"
                },
                "crab3@vocms0137.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0137"
                },              
                "crab3@vocms0107.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0107"
                },     
                "crab3@vocms0119.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0119"
                },                     
                "crab3@vocms0120.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0120"
                },
                "crab3@vocms0194.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0194"
                },
                "crab3@vocms0195.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0195"
                },
                "crab3@vocms0196.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0196"
                },
                "crab3@vocms0197.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0197"
                },
                "crab3@vocms0198.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0198"
                },
                "crab3@vocms0199.cern.ch" : {
                    "proxiedurl": "https://cmsweb-testbed.cern.ch/scheddmon/0199"
                }
            },
            "htcondorPool" : "cmsgwms-collector-global.cern.ch,cmssrv221.fnal.gov",
            "asoConfig" : [
                {"couchURL" : "https://cmsweb-testbed.cern.ch/couchdb", "couchDBName" : "asynctransfer1"}
            ]
        },
        "compatible-version" : ["3.3.11", "3.3.12", "3.3.13.rc4","3.3.1602","3.3.1603","3.3.1604", "3.3.1605", "3.3.1606" ],
        "banned-out-destinations" : ["T1_*"]
    },
    "cmsweb-prod": {
        "delegate-dn": [
            "/DC=ch/DC=cern/OU=computers/CN=vocms(045|052|021|031|0118|0118a|0119).cern.ch"
        ],
        "backend-urls" : {
            "cacheSSL" : "https://cmsweb.cern.ch/crabcache",
            "baseURL" : "https://cmsweb.cern.ch/crabcache",
            "htcondorSchedds" : {
                "crab3@vocms021.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/021"
                },
                "crab3-4@vocms066.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/066"
                },
                "crab3-8@vocms0106.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0106"
                },
                "crab3@vocms0121.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0121"
                },
                "crab3@vocms0122.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0122"
                },
                "crab3@vocms0155.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0155"
                },
                "crab3@vocms0144.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0144"
                },
                "crab3@vocms0137.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0137"
                },              
                "crab3@vocms0107.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0107"
                },     
                "crab3@vocms0119.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0119"
                },                     
                "crab3@vocms0120.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0120"
                },
                "crab3@vocms0194.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0194"
                },
                "crab3@vocms0195.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0195"
                },
                "crab3@vocms0196.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0196"
                },
                "crab3@vocms0197.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0197"
                },
                "crab3@vocms0198.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0198"
                },
                "crab3@vocms0199.cern.ch" : {
                    "proxiedurl": "https://cmsweb.cern.ch/scheddmon/0199"
                }
            },
            "htcondorPool" : "cmsgwms-collector-global.cern.ch,cmssrv221.fnal.gov",
            "ASOURL" : "https://cmsweb.cern.ch/couchdb",
            "asoConfig" : [
                {"couchURL" : "https://cmsweb.cern.ch/couchdb2", "couchDBName" : "asynctransfer"}
            ]
        },
        "compatible-version" : ["3.3.1506", "3.3.1507", "3.3.1509","3.3.1510","3.3.1511","3.3.1512", "3.3.1602", "3.3.1603", "3.3.1604","3.3.1605", "3.3.1606" ],
        "banned-out-destinations" : ["T1_*"]
    }
}
